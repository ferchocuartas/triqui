﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking.NetworkSystem;

public class jugar : MonoBehaviour {

	public GUITexture[] cuadro;
	public Texture marca_x;
	public Texture marca_o;
	public GUITexture recargar;
	public GUIText texto;
	public GUIText puntosMaquina_texto;
	public GUIText puntosJugador_texto;
	public GUIText empates_texto;
	public GameObject nombre;
	public GUITexture marca_maquina;
	public GUITexture marca_jugador;
	public GameObject audio_perder;
	public GameObject audio_empate;
	public Boolean creditos;

	private int[] tablero;
	private int touch;
	private int turno;
	private int jugada_maquina;
	private int jugada;
	private int empieza;
	private int puntos_maquina;
	private int puntos_jugador;
	private int empates;
	private float tiempo1;
	private float tiempo2;
	private int cont;

	void Start () {
		puntos_maquina = 0;
		puntos_jugador = 0;
		empates = 0;
		tiempo1 = 0;
		tiempo2 = 0;
		cont = 0;
		tablero = new int[9];
		empieza=1;
		touch=0;
		turno=1;
		texto.text = "TU TURNO";
		jugada=0;
		for(int i=0;i<9;i++){
			cuadro[i].texture=null;
			tablero [i] = -1;
		}
		marca_maquina.texture=marca_o;
		marca_jugador.texture=marca_x;
	}

	void Update () {
		if (Input.touchCount > 0 && !creditos) {
			for (var i = 0; i < Input.touchCount; i++) {
				for(var j=0;j<9;j++){
					if(cuadro[j].HitTest(Input.GetTouch(i).position) && touch == 0 && turno==1 && tablero[j]==-1 && jugada<9){
						touch=1;
						GetComponent<AudioSource>().Play();
						cuadro[j].texture=marca_jugador.texture;
						turno=0;
						tablero[j]=0;
						jugada=jugada+1;
						if(ganar()==0){
							texto.text="GANASTE";
							puntos_jugador = puntos_jugador + 1;
							puntosJugador_texto.text = "" + puntos_jugador;
						}
						else{
							movimiento_maquina();
						}
					}
				}
				if(recargar.HitTest(Input.GetTouch(i).position) && touch==0){
					puntos_maquina = 0;
					puntos_jugador = 0;
					empates = 0;
					touch=1;
					empieza = 0;
					puntosMaquina_texto.text = "00";
					puntosJugador_texto.text = "00";
					empates_texto.text = "00";
					reiniciar();
					json ob = nombre.GetComponent<json> ();
					ob.StartCoroutine(ob.nombre());
				}
				if (texto.HitTest (Input.GetTouch (i).position) && touch == 0 && fin_partida ()) {
					reiniciar ();
					texto.enabled = true;
					touch = 1;
				}

			}
		}
		else{
			touch=0;
		}
		if(jugada==9 && ganar()== -1){
			jugada=jugada+1;
			audio_empate.transform.GetComponent<AudioSource> ().Play ();
			texto.text="EMPATE";
			empates = empates + 1;
			empates_texto.text = "" + empates;
		}
		if (fin_partida ()) {
			tiempo1 += Time.deltaTime;
			if (tiempo1 > 1.5) {
				texto.enabled = false;
				tiempo2 += Time.deltaTime;
				if (tiempo2 > 0.5) {
					texto.enabled = true;
					tiempo1 = 0;
					tiempo2 = 0;
					cont = cont + 1;
					if (cont > 2) {
						reiniciar ();
					}
				}
			}
		}


	}

	void reiniciar (){
		jugada=0;
		cont = 0;
		texto.text="";
		for(var i=0;i<9;i++){
			cuadro[i].texture=null;
			tablero[i]=-1;
		}

		if(empieza==1){
			marca_maquina.texture=marca_x;
			marca_jugador.texture=marca_o;
			empieza=0;
			movimiento_maquina();
		}
		else{
			marca_maquina.texture=marca_o;
			marca_jugador.texture=marca_x;
			empieza=1;
			texto.text = "TU TURNO";
		}
		turno = 1;

	}
	int ganar(){
		//horizontal
		if(tablero[0]==tablero[1] && tablero[0]==tablero[2] && tablero[0]!=-1){
			return tablero[0];
		}
		else if(tablero[3]==tablero[4] && tablero[3]==tablero[5] && tablero[3]!=-1){
			return tablero[3];
		}
		else if(tablero[6]==tablero[7] && tablero[6]==tablero[8] && tablero[6]!=-1){
			return tablero[6];
		}
		//vertical
		else if(tablero[0]==tablero[3] && tablero[0]==tablero[6] && tablero[0]!=-1){
			return tablero[0];
		}
		else if(tablero[1]==tablero[4] && tablero[1]==tablero[7] && tablero[1]!=-1){
			return tablero[1];
		}
		else if(tablero[2]==tablero[5] && tablero[2]==tablero[8] && tablero[2]!=-1){
			return tablero[2];
		}
		//diagonales
		else if(tablero[0]==tablero[4] && tablero[0]==tablero[8] && tablero[0]!=-1){
			return tablero[0];
		}
		else if(tablero[2]==tablero[4] && tablero[2]==tablero[6] && tablero[2]!=-1){
			return tablero[2];
		}
		else{
			return-1;
		}
	}

	///algoritmo mini-max

	Boolean tablero_completo(){
		for (var i = 0; i < 9; i++) {
			if (tablero [i] == -1) {
				return false;
			}
		}
		return true;
	}

	void movimiento_maquina (){
		if(jugada<9){
			int x = 0;
			int v = int.MinValue;
			int aux;
			for (var i = 0; i < 9; i++) {
				if (tablero [i] == -1) {
					tablero[i]=1;
					aux = min ();
					if (aux > v) {
						v = aux;
						x = i;
					}
					tablero[i]=-1;
				}
			}
			tablero[x]=1;
			jugada_maquina = x;
			cuadro[jugada_maquina].texture=marca_maquina.texture;

			texto.text = "TU TURNO";
			jugada=jugada+1;
			if(ganar()==1){
				audio_perder.transform.GetComponent<AudioSource> ().Play ();
				texto.text="PERDISTE";
				puntos_maquina = puntos_maquina + 1;
				puntosMaquina_texto.text = "" + puntos_maquina;
			}
			else{
				turno=1;
			}
		}

	}
	Boolean fin_partida(){
		return tablero_completo () || ganar () != -1;
	}
	int max(){
		if (fin_partida()) {
			if (ganar () != -1) {
				return -1;
			} else {
				return 0;
			}
		}
		int v = int.MinValue;
		int aux;
		for (var i = 0; i < 9; i++) {
			if (tablero [i] == -1) {
				tablero[i]=1;
				aux = min ();
				if (aux > v) {
					v = aux;
				}
				tablero[i]=-1;
			}
		}
		return v;
	}

	int min(){
		if (fin_partida()) {
			if (ganar () != -1) {
				return 1;
			} else {
				return 0;
			}
		}
		int v = int.MaxValue;
		int aux;
		for (var i = 0; i < 9; i++) {
			if (tablero [i] == -1) {
				tablero[i]=0;
				aux = max ();
				if (aux < v) {
					v = aux;
				}
				tablero[i]=-1;
			}
		}
		return v;
	}


}
