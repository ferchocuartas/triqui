﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class json : MonoBehaviour {
	private GUIText txtRef;
	private IEnumerator coroutine;
	// Use this for initialization
	void Start () {
		coroutine=nombre ();
		StartCoroutine (coroutine);
	}

	public IEnumerator nombre(){
		string url = "http://jsonplaceholder.typicode.com/users";
		WWW www = new WWW(url);
		yield return www;

		if (www.error == null)
		{
			JSONObject jo = new JSONObject(www.text);
			int posicion= Random.Range(0,jo.Count);
			var obj = jo[posicion];
			var company = obj ["company"];
			txtRef = GetComponent<GUIText>();
			txtRef.text ="USERNAME-"+obj["username"]+"\nCOMPANY-"+company["name"];
		}
		else
		{
			txtRef.text = "SIN CONEXION";
			Debug.Log("ERROR: " + www.error);
		} 
	}
				
}
