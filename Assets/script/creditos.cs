﻿using UnityEngine;
using System.Collections;

public class creditos : MonoBehaviour {
	public GUITexture boton_creditos;
	public GUITexture menu;
	public GameObject fondo;
	private jugar ob;
	// Use this for initialization
	void Start () {
		ob = fondo.GetComponent<jugar> ();
		ob.creditos = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.touchCount > 0) {
			for (var i = 0; i < Input.touchCount; i++) {
				if(boton_creditos.HitTest(Input.GetTouch(i).position)){
					ob.creditos = true;
					transform.position= new Vector3(0.5f,0.5f,2);
					menu.transform.position=new Vector3(0.5f,0.05f,3);
				}
				if (menu.HitTest (Input.GetTouch (i).position)) {
					ob.creditos = false;
					transform.position= new Vector3(0.5f,0.5f,-5);
					menu.transform.position=new Vector3(0.5f,0.05f,-5);
				}
			}
		}
	}
}
